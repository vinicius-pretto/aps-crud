import { ApsCrudPage } from './app.po';

describe('aps-crud App', function() {
  let page: ApsCrudPage;

  beforeEach(() => {
    page = new ApsCrudPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
