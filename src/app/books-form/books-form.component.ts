import { Component, OnInit } from '@angular/core';
import { Book } from "app/book";
import { BookService } from "app/book-service";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-books-form',
  templateUrl: './books-form.component.html',
  styleUrls: ['./books-form.component.css']
})
export class BooksFormComponent implements OnInit {
  title = 'Books Form';
  book:Book;
  id:number;
  
  constructor(
    private service:BookService, 
    private router:Router,
    private route:ActivatedRoute) {
    
  }

  ngOnInit() {
      this.id = this.route.snapshot.params['id'];

      if (isNaN(this.id)) {
        this.book = new Book;
      } 
      else {
        this.book = Object.assign({}, this.service.findBy(this.id));
      }
  }

  create() {
    if (isNaN(this.id)) {
      this.service.create(this.book);
      this.book = new Book();
    } 
    else {
      this.service.update(this.id, this.book);
    }
    this.router.navigate(['/books']);
  }

  cancel() {
    this.router.navigate(['/books']);
  }
}
