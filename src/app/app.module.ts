import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { BooksTableComponent } from './books-table/books-table.component';
import { BooksFormComponent } from './books-form/books-form.component';
import { BookService } from "app/book-service";
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'books', pathMatch: 'full' },
  { path: 'books', component: BooksTableComponent },
  { path: 'edit/:id', component: BooksFormComponent },
  { path: 'new', component: BooksFormComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    BooksTableComponent,
    BooksFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  providers: [BookService],
  bootstrap: [AppComponent]
})
export class AppModule { }
