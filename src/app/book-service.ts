import { Injectable } from '@angular/core';
import { Book } from "app/book";

@Injectable()
export class BookService {
  books: Book[] = [
    {
      id: 1,
      name: 'Clean Code',
      author: 'Robert C.Martin'
    },
    {
      id: 2,
      name: 'Web Development with MongoDB and NodeJS',
      author: 'Bruno Joseph D\'mello'
    }
  ]

  autoIncrement: number = 3;

  constructor() { }
  
  create(book:Book) {
    book.id = this.autoIncrement++;
    this.books.push(book);
  }

  findAll() {
    return this.books;
  }

  findBy(id:number) {
    return this.books.find(book => book.id === id);
  }

  remove(book:Book) {
    const index = this.books.indexOf(book, 0);
    const containsBook = index > -1;
    
    if (containsBook) {
      this.books.splice(index, 1);  
    }
  }

  update(id:number, book:Book) {
    const index = this.books.indexOf(this.findBy(Number(id)), 0);
    this.books[index] = book;
  }
}
