import { Component, OnInit } from '@angular/core';
import { Book } from "app/book";
import { BookService } from "app/book-service";

@Component({
  selector: 'app-books-table',
  templateUrl: './books-table.component.html',
  styleUrls: ['./books-table.component.css']
})
export class BooksTableComponent implements OnInit {
  title = 'IT Library';
  books:Book[] = [];

  constructor(private service:BookService) {

  }
  
  ngOnInit() {
    this.books = this.service.findAll();
  }

  remove(book:Book) {
    this.service.remove(book);
  }
}
